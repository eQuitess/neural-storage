package com.mizy.neuralstorage

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mizy.neuralstorage.api.NeuralStorageService
import com.mizy.neuralstorage.api.model.ImageResponse
import com.mizy.neuralstorage.api.model.Tags
import com.mizy.neuralstorage.api.model.TagsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_tags.*
import java.math.BigDecimal


class TagsActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "TagsActivity"
        const val TAGS_RESP: String = "TAGS_RESP"
    }

    private var imageResp: ImageResponse? = null
    private var disposable: Disposable? = null

    private var correctedTags: ArrayList<String> = ArrayList()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tags)

        imageResp = intent.getSerializableExtra(MainActivity.IMAGE_RESP) as ImageResponse
        correctedTags = imageResp?.generatedTags!!

        //instantiate custom adapter
        val adapter = MyCustomAdapter(correctedTags, this)
        listView.adapter = adapter

        tagRowAdd.setOnClickListener {
            correctedTags.add("tag")
            adapter.notifyDataSetChanged()
        }
        processTagsBt.setOnClickListener { onProcessTags() }
    }

    private fun onProcessTags() {
        Log.d(TagsActivity.TAG, "Process tags")

        processTagsBt.isEnabled = false

        val progressDialog = ProgressDialog(this@TagsActivity, R.style.MainTheme)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Processing tags...")
        progressDialog.show()

        val tags = Tags(
                tagsImage = imageResp?.image?.imageId!!,
                tagsGenerated = imageResp?.generatedTags!!,
                tagsCorrected = correctedTags,
                //TODO: Remove rating from tags form
                tagsRating = BigDecimal.ZERO
        )

        disposable = NeuralStorageService.instance
                .processTags(tags, getSharedPreferences(getString(R.string.pref), Context.MODE_PRIVATE).getString(getString(R.string.token), ""))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {
                    processTagsBt.isEnabled = true
                    progressDialog.dismiss()
                }
                .subscribe(
                        { result -> onRequestSuccess(result) },
                        { error -> onRequestFailure(error) }
                )
    }

    private fun onRequestSuccess(tagsResp: TagsResponse) {
        val intent = Intent(this, DescActivity::class.java).apply {
            putExtra(TagsActivity.TAGS_RESP, tagsResp)
        }
        startActivity(intent)
        finish()
    }

    private fun onRequestFailure(error: Throwable) {
        Log.e(TAG, "Tags process error!", error)
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
    }

    inner class MyCustomAdapter(private val list: ArrayList<String>, private val context: Context) : BaseAdapter(), ListAdapter {

        override fun getCount(): Int {
            return list.size
        }

        override fun getItem(pos: Int): Any {
            return list[pos]
        }

        override fun getItemId(pos: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view: View? = convertView
            if (view == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.tag_row_list, null)
            }

            //Handle TextView and display string from your list
            val listItemText = view!!.findViewById(R.id.tagRowText) as EditText
            listItemText.setText(list[position])

            listItemText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    list[position] = p0.toString()
                }

                override fun afterTextChanged(p0: Editable?) {}
            })

            //Handle buttons and add onClickListeners
            val deleteBtn = view.findViewById(R.id.tagRowDel) as ImageButton

            deleteBtn.setOnClickListener {
                list.removeAt(position)
                notifyDataSetChanged()
            }

            return view
        }
    }
}