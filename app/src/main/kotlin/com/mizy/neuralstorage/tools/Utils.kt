package com.mizy.neuralstorage.tools

import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.util.Base64
import java.io.ByteArrayOutputStream


object Utils {

    fun getBase64String(bitmap: Bitmap?): String {
        val stream = ByteArrayOutputStream()
        bitmap?.compress(CompressFormat.JPEG, 70, stream)
        val byteFormat = stream.toByteArray()

        return Base64.encodeToString(byteFormat, Base64.NO_WRAP)
    }

}