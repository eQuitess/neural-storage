package com.mizy.neuralstorage

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.SeekBar
import android.widget.Toast
import com.mizy.neuralstorage.api.NeuralStorageService
import com.mizy.neuralstorage.api.model.Description
import com.mizy.neuralstorage.api.model.TagsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_desc.*
import java.math.BigDecimal


class DescActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "DescActivity"
    }

    private var tagsResp: TagsResponse? = null
    private var disposable: Disposable? = null

    private var correctedDesc: String = "Description"
    private var descRating: BigDecimal = BigDecimal.ZERO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desc)

        tagsResp = intent.getSerializableExtra(TagsActivity.TAGS_RESP) as TagsResponse
        correctedDesc = tagsResp?.generatedDescription!!

        descEditText.setText(correctedDesc)
        descEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                correctedDesc = p0.toString()
            }

            override fun afterTextChanged(p0: Editable?) {}
        })

        ratingSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                ratingView.text = "Description rating: $p1"
                descRating = BigDecimal(p1)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })

        val progress = ratingSeekBar.progress
        ratingView.text = "Description rating: $progress"
        descRating = BigDecimal(progress)

        processDescBt.setOnClickListener { onProcessDesc() }
    }

    private fun onProcessDesc() {
        Log.d(DescActivity.TAG, "Process description")

        processDescBt.isEnabled = false

        val progressDialog = ProgressDialog(this@DescActivity, R.style.MainTheme)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Processing description...")
        progressDialog.show()

        val description = Description(
                descTags = tagsResp?.tags?.tagsId!!,
                descGenerated = tagsResp?.generatedDescription!!,
                descCorrected = correctedDesc,
                descRating = descRating
        )

        disposable = NeuralStorageService.instance
                .processDescription(description, getSharedPreferences(getString(R.string.pref), Context.MODE_PRIVATE).getString(getString(R.string.token), ""))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {
                    processDescBt.isEnabled = true
                    progressDialog.dismiss()
                }
                .subscribe(
                        { result -> onRequestSuccess(result) },
                        { error -> onRequestFailure(error) }
                )
    }

    private fun onRequestSuccess(descriptionResp: Description) {
        Toast.makeText(this, "Rating success. Thanks for contirbuting!", Toast.LENGTH_LONG).show()
        finish()
    }

    private fun onRequestFailure(error: Throwable) {
        Log.e(TAG, "Description process error!", error)
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
