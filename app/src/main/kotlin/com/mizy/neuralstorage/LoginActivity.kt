package com.mizy.neuralstorage


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.mizy.neuralstorage.api.NeuralStorageService
import com.mizy.neuralstorage.api.model.Credentials
import com.mizy.neuralstorage.api.model.TokenResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "LoginActivity"
    }

    private var disposable: Disposable? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginBt.setOnClickListener { login() }
    }

    private fun login() {
        Log.d(TAG, "Login")

        loginBt.isEnabled = false

        val progressDialog = ProgressDialog(this@LoginActivity, R.style.MainTheme)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Authenticating...")
        progressDialog.show()

        val credentials = Credentials(
                username = inputUsername.text.toString(),
                password = inputPassword.text.toString()
        )

        disposable = NeuralStorageService.instance
                .authLogin(credentials)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {
                    loginBt.isEnabled = true
                    progressDialog.dismiss()
                }
                .subscribe(
                        { result -> onRequestSuccess(result) },
                        { error -> onRequestFailure(error) }
                )
    }

    override fun onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true)
    }

    private fun onRequestSuccess(tokenResp: TokenResponse) {
        val sharedPref = this.getSharedPreferences(getString(R.string.pref), Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(getString(R.string.token), "Bearer ${tokenResp.token}")
            commit()
        }

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun onRequestFailure(error: Throwable) {
        Log.e(TAG, "Login error!", error)
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
