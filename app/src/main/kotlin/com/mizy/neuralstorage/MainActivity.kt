package com.mizy.neuralstorage


import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.mizy.neuralstorage.api.NeuralStorageService
import com.mizy.neuralstorage.api.model.Image
import com.mizy.neuralstorage.api.model.ImageResponse
import com.mizy.neuralstorage.tools.ImagePicker
import com.mizy.neuralstorage.tools.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
        const val IMAGE_RESP: String = "IMAGE_RESP"
        const val REQUEST_IMAGE: Int = 100
        const val REQUEST_PERMISSION: Int = 200
    }

    private var imageBitmap: Bitmap? = null
    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET),
                    REQUEST_PERMISSION)
        }

        imageViewText.setOnClickListener { onPickImage() }
        processBt.setOnClickListener { onProcessImage() }
    }

    private fun onProcessImage() {
        Log.d(MainActivity.TAG, "Process image")

        processBt.isEnabled = false

        val progressDialog = ProgressDialog(this@MainActivity, R.style.MainTheme)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Processing image...")
        progressDialog.show()

        val image = Image(
                imageName = android.os.Build.MODEL + UUID.randomUUID(),
                imageData = Utils.getBase64String(imageBitmap)
        )

        disposable = NeuralStorageService.instance
                .processImage(image, getSharedPreferences(getString(R.string.pref), Context.MODE_PRIVATE).getString(getString(R.string.token), ""))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally {
                    processBt.isEnabled = true
                    progressDialog.dismiss()
                }
                .subscribe(
                        { result -> onRequestSuccess(result) },
                        { error -> onRequestFailure(error) }
                )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSION && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Thanks for granting Permission", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_IMAGE -> {
                if (resultCode == RESULT_OK) {
                    imageBitmap = ImagePicker.getImageFromResult(this, resultCode, data)
                    imageView.setImageBitmap(imageBitmap)
                    processBt.visibility = View.VISIBLE
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "You cancelled the operation", Toast.LENGTH_SHORT).show()
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onRequestSuccess(imageResp: ImageResponse) {
        val intent = Intent(this, TagsActivity::class.java).apply {
            putExtra(IMAGE_RESP, imageResp)
        }
        startActivity(intent)
    }

    private fun onRequestFailure(error: Throwable) {
        Log.e(TAG, "Image process error!", error)
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
    }

    private fun onPickImage() {
        val chooseImageIntent = ImagePicker.getPickImageIntent(this)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        startActivityForResult(chooseImageIntent, REQUEST_IMAGE)
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}