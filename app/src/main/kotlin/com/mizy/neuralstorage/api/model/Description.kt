package com.mizy.neuralstorage.api.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Description(
        @SerializedName("description_id")
        var descId: Int? = null,
        @SerializedName("description_tags")
        var descTags: Int,
        @SerializedName("description_generated")
        var descGenerated: String,
        @SerializedName("description_corrected")
        var descCorrected: String,
        @SerializedName("description_rating")
        var descRating: BigDecimal,
        @SerializedName("description_created")
        var descCreated: String? = null
)