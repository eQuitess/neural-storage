package com.mizy.neuralstorage.api.model

import com.google.gson.annotations.SerializedName

data class ImageResponse(
        @SerializedName("image")
        val image: Image,
        @SerializedName("generated_tags")
        val generatedTags: ArrayList<String>
): java.io.Serializable