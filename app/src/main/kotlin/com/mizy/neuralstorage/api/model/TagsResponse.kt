package com.mizy.neuralstorage.api.model

import com.google.gson.annotations.SerializedName

data class TagsResponse(
        @SerializedName("tags")
        val tags: Tags,
        @SerializedName("generated_desc")
        val generatedDescription: String
): java.io.Serializable