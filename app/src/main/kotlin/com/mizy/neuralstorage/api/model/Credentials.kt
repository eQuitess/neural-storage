package com.mizy.neuralstorage.api.model

data class Credentials(
        val username: String,
        val password: String
)
