package com.mizy.neuralstorage.api

import com.mizy.neuralstorage.api.model.*
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST


interface NeuralStorageService {

    @POST("/auth/login")
    fun authLogin(@Body credentials: Credentials): Observable<TokenResponse>

    @POST("/api/image/")
    fun processImage(@Body image: Image, @Header("Authorization") token: String): Observable<ImageResponse>

    @POST("/api/tag/")
    fun processTags(@Body image: Tags, @Header("Authorization") token: String): Observable<TagsResponse>

    @POST("/api/desc/")
    fun processDescription(@Body image: Description, @Header("Authorization") token: String): Observable<Description>

    companion object {
        private const val HOST: String = "https://neural-vista-231223.appspot.com"

        val instance: NeuralStorageService by lazy {
            val retrofit = Retrofit.Builder()
                    .baseUrl(HOST)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            retrofit.create(NeuralStorageService::class.java)
        }
    }
}