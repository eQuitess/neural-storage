package com.mizy.neuralstorage.api.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Tags(
        @SerializedName("tags_id")
        var tagsId: Int? = null,
        @SerializedName("tags_image")
        var tagsImage: Int,
        @SerializedName("tags_generated")
        var tagsGenerated: ArrayList<String>,
        @SerializedName("tags_corrected")
        var tagsCorrected: ArrayList<String>,
        @SerializedName("tags_rating")
        var tagsRating: BigDecimal,
        @SerializedName("tags_created")
        var tagsCreated: String? = null
) : java.io.Serializable