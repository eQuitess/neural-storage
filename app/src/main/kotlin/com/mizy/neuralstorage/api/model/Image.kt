package com.mizy.neuralstorage.api.model

import com.google.gson.annotations.SerializedName

data class Image(
        @SerializedName("image_id")
        val imageId: Int? = null,
        @SerializedName("image_name")
        val imageName: String,
        @SerializedName("image_data")
        val imageData: String,
        @SerializedName("image_created")
        val imageCreated: String? = null
) : java.io.Serializable