package com.mizy.neuralstorage.api.model

data class TokenResponse(val token: String)